package com.pariyakorn.midterm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class Bathroom {
    private int width;
    private int height;
    private Babypig babypig;
    private Shampoo shampoo;
    private Foam foam;

    public Bathroom(int x,int y) {
        this.width = x;
        this.height = y;

    }
    public void showMap(){
        for(int y = 0 ; y < height ; y++){
            for(int x = 0; x < width ; x++){
               if(babypig.isOn(x,y)){
                    System.out.print("p");
                }else if(shampoo.isOn(x, y)){
                    System.out.print("s");
                }else if(foam.isOn(x, y)){
                    System.out.print("f");    
                }else{
                    System.out.print("-");
                }
            }
            System.out.println("");
        }
    }

    public boolean inMap(int x ,int y){
        // x -> 0-(width-1),y -> 0-(height-1)
        return (x >= 0 && x < width ) && (y >= 0 && y < height);
    }
    public boolean isShampoo(int x ,int y){
        return shampoo.isOn(x,y);
    }
    public boolean isFoam(int x ,int y){
        return foam.isOn(x,y);
    }
    public void setShampoo(Shampoo shampoo) {
        this.shampoo = shampoo;
    }

    public void setBabypig(Babypig babypig){
        this.babypig = babypig;
    }
    public void setFoam(Foam foam) {
        this.foam = foam;
    }

}
